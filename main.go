package main

import (
	"log"
	"net/http"

	"github.com/twalters/fantasyedge/app/router"
)

func main() {
	log.Fatal(http.ListenAndServe("localhost:5432", router.Router))
}
