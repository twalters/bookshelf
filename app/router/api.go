package router

import (
	"io"
	"net/http"

	"fmt"
)

func GetBookById(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "Hello world!")
	// book := model.Book.GetBookById()
	// json.NewEncoder(w).Encode(book)
}

func GetBooks() {

}

func init() {
	fmt.Println("Adding API routes")
	APIRouter.HandleFunc("/book", GetBookById).Methods("GET")
}
