package router

import (
	"io"
	"net/http"

	"github.com/gorilla/mux"
)

var Router = mux.NewRouter()
var APIRouter = Router.PathPrefix("/api/v1").Subrouter()

func hello(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Hello world!")
}

func init() {
	Router.HandleFunc("/", hello).Methods("GET")
}
