package model

type Book struct {
	ID        int        `json:"id,omitempty"`
	Title     string     `json:"title,omitempty"`
	Author      int        `json:"author,omitempty"` // Reference Author by ID
    Sumary string `json:"summary,omitempty"`
}
