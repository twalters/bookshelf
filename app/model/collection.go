package model

type Collection struct {
    ID        int        `json:"id,omitempty"`
    Name string `json:"name,omitempty"`
    Books []int `json:"books,omitempty"` // Reference by Book IDs
}