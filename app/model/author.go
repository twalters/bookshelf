package model

type Author struct {
    ID        int        `json:"id,omitempty"`
    Name string `json:"name,omitempty"`
}